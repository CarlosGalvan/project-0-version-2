# Project 0
For Project 0, you will be building a console-based banking application. Presentation date: June 30th, 2021 (takes the place of QC)
## User Stories
As a user, I can:
- [x] register a new user account with the system (must be secured with a password)
- [x] login with my existing credentials
- [x] create at least one account
- [x] deposit funds into an account
- [x] withdraw funds from an account (no overdrafting!)
- [x] view the balance of my account(s) (all balance displays must be in proper currency format)
#### Suggested Bonus User Stories
As a user I can:
- [ ] view the transaction history for an account
- [x] create multiple accounts per user (checking, savings, etc.)
- [ ] share a joint account with another user
- [ ] transfer money between accounts
## Minimum Features
- [x] Basic validation (proper format, no negative deposits/withdrawals, no overdrafting, etc.) 
- [x] All exceptions are properly caught and handled
- [x] Proper use of OOP principles
- [ ] Documentation (all classes and methods have basic documentation)
- [x] SQL Data Persistance (multiple tables; all 3NF (normal form))
#### Bonus Features
- [ ] Unit tests for persistance-layer classes
- [ ] Logging messages and exceptions to a file
## Tech Stack
- [x] Java 8
- [x] Apache Maven
- [x] PostgreSQL deployed on AWS RDS
- [x] Git SCM (on GitLab)
## Init Instructions
- Create a new repository within your subgroup to maintain your code, pushing your progress regularly
## Presentation
- [ ] finalized version of application must be pushed to your repository by the 10am EST on the presentation date
- [ ] 5 minute live demonstration of the implemented features