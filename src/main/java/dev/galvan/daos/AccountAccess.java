package dev.galvan.daos;

import dev.galvan.models.*;

import java.util.List;

/*
This DAO interface holds all the method that corralates with user_accounts table in my postgres DB
As such these methods should have the capability to
> register an account
> login into an existing account
> create a bank account (savings or checking)
> access current balance
> get bank statements
> join a joint account with a nother user
>


 */
public interface AccountAccess {

    public boolean registerUser(UserAccount user);

    boolean  verifyAccountExist(String email);

    public LocalStorage loginAccount(LoginPackage input, LocalStorage localStorage);

    boolean verifyUserAndPassword();

    public BankAccount createNewBankAccount(BankAccount bankAccount);


    public List<BankAccount> getAllAssociatedBankAccounts(UserAccount userAccount);




}
