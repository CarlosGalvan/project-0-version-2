package dev.galvan.daos;

import dev.galvan.models.BankAccount;
import dev.galvan.models.BankStatements;
import dev.galvan.models.UserAccount;

import java.util.List;

/*
    This is the BankTransation interface that will correlate with the both bank_account table and bank transation table
    the created methods will serve the purpose of:
    > Depositing Funds
    > Withdrawing Funds
    > View the balance
    > Generate bank statement (transaction history)

 */
public interface BankTransactions {

    double depositFunds(BankStatements bankStatements, double balace);

    void createStatement(BankStatements bankStatements);
    double withdrawFunds(BankStatements bankStatements, double balace);

    double checkBalance(BankAccount bankAccount);
    boolean verifyFunds(UserAccount user, double amount);

    List<BankStatements> bankStatement(BankAccount bankAccount);

}
