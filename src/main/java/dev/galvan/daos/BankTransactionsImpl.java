package dev.galvan.daos;

import dev.galvan.models.BankAccount;
import dev.galvan.models.BankStatements;
import dev.galvan.models.UserAccount;
import dev.galvan.util.ConnectUtil;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class BankTransactionsImpl implements BankTransactions{
    @Override
    public double depositFunds(BankStatements bankStatements, double balance) {
        int success = 0;
        boolean greenLight = false;

        String balanceQuery = "UPDATE public.bank_account " +
                "SET balance = ? " +
                "WHERE user_account_id = ? AND account_number =  ?;";


//        if (greenLight) {
            try (Connection connection = ConnectUtil.getConnection();
                 PreparedStatement preparedStatement = connection.prepareStatement(balanceQuery);
            ) {
                preparedStatement.setInt(1,(int) ((bankStatements.getTransactionAmount() + balance) * 100));
                preparedStatement.setInt(2, bankStatements.getUserAccountId());
                preparedStatement.setInt(3, bankStatements.getAccountNumber());

//                System.out.println("Loading...");
                success = preparedStatement.executeUpdate();
                if (success > 0) {
                    createStatement(bankStatements);
                    System.out.println("Deposit Successful");
                    return bankStatements.getTransactionAmount() + balance;
                } else {
                    System.out.println("Something went wrong; Try again");
                    return bankStatements.getTransactionAmount();
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        return balance;
    }

    @Override
    public void createStatement(BankStatements bankStatements) {
        String sql = "INSERT INTO public.bank_transaction( " +
                " account_number, transaction_type, base_amount, transaction_date, user_account_id) " +
                " VALUES (?, ?, ?, ?, ?);";
        try (Connection connection = ConnectUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, bankStatements.getAccountNumber());
            preparedStatement.setString(2, bankStatements.getTransactionType());
            preparedStatement.setDouble(3, (int) ( bankStatements.getTransactionAmount() * 100));
            preparedStatement.setObject(4, bankStatements.getTransactionDate());
            preparedStatement.setInt(5, bankStatements.getUserAccountId());

            preparedStatement.executeUpdate();
//            System.out.println("Loading..");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public double withdrawFunds(BankStatements bankStatements, double balance) {

        int success = 0;

        String balanceQuery = "UPDATE public.bank_account " +
                "SET balance = ? " +
                "WHERE user_account_id = ? AND account_number =  ?;";


//        if (greenLight) {
        try (Connection connection = ConnectUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(balanceQuery);
        ) {
            preparedStatement.setInt(1,(int) ((balance - bankStatements.getTransactionAmount() ) * 100));
            preparedStatement.setInt(2, bankStatements.getUserAccountId());
            preparedStatement.setInt(3, bankStatements.getAccountNumber());

//            System.out.println("Loading...");
            success = preparedStatement.executeUpdate();
            if (success > 0) {
                createStatement(bankStatements);
//                System.out.println("Withdraw Successful");
                return balance - bankStatements.getTransactionAmount();
            } else {
                System.out.println("Something went wrong; Try again");
                return bankStatements.getTransactionAmount();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return balance;
    }



    @Override
    public double checkBalance(BankAccount bankAccount) {
    String sql = "SELECT balance FROM public.bank_account WHERE user_account_id = " + bankAccount.getUserAccountId() + " AND account_number = " + bankAccount.getAccountNumber()+  ";";
        try (Connection connection = ConnectUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
            while (rs.next()) {
                double total = rs.getInt("balance") * 0.01;
//                System.out.printf("Your current balance is: $%.2f.%n", total);
                return total;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean verifyFunds(UserAccount user, double amount) {
        return false;
    }

    @Override
    public List<BankStatements> bankStatement(BankAccount bankAccount) {
        String sql = "SELECT transaction_id, account_number, transaction_type, base_amount, transaction_date, user_account_id FROM public.bank_transaction WHERE account_number = " +  bankAccount.getAccountNumber() + ";";
        try (Connection connection = ConnectUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {


            List<BankStatements> allTranscation = new ArrayList<>();
            while(rs.next()){
                BankStatements current = new BankStatements();
                current.setTransactionId(rs.getInt("transaction_id"));
                current.setAccountNumber(rs.getInt("account_number"));
                current.setTransactionType(rs.getString("transaction_type"));
                current.setTransactionAmount(rs.getInt("base_amount") * 0.01);

                Time time = rs.getTime("transaction_date");
                Timestamp timestamp = new Timestamp(time.getTime());
                current.setTransactionDate(timestamp.toLocalDateTime());
                current.setUserAccountId(rs.getInt("user_account_id"));
                allTranscation.add(current);
            }
            return allTranscation;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }
}
