package dev.galvan.models;

import java.util.Objects;

public class LocalStorage {
    private boolean running;
    private boolean loggedIn;
    private boolean exitClause;
    private boolean welcome;

    private UserAccount userAccount;

    public LocalStorage() {
        this.running = false;
        this.loggedIn = false;
        this.welcome = true;

    }

    public void setRunning(boolean running) {
        this.running = running;
        this.welcome = true;
    }

    public boolean isWelcome() {
        return welcome;
    }

    public void setWelcome(boolean welcome) {
        this.welcome = welcome;
    }

    public boolean isExitClause() {
        return exitClause;
    }

    public void setExitClause(boolean exitClause) {
        this.exitClause = exitClause;
    }

    public LocalStorage(boolean run) {
        this.running = run;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRun(boolean run) {
        this.running = run;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalStorage that = (LocalStorage) o;
        return running == that.running && loggedIn == that.loggedIn && Objects.equals(userAccount, that.userAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(running, loggedIn, userAccount);
    }

    @Override
    public String toString() {
        return "LocalPackage{" +
                "run=" + running +
                ", loggedIn=" + loggedIn +
                ", userAccount=" + userAccount +
                '}';
    }
}
