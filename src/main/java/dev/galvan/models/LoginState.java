package dev.galvan.models;
/*
This class contains the log in status and the asscociated account that is being accessed.
 */
import java.util.Objects;

public class LoginState {
    private boolean logingStatus;
    private UserAccount userAccount;

    public LoginState() {
        this.logingStatus = false;
    }

    public LoginState(boolean logingStatus, UserAccount userAccount) {
        this.logingStatus = logingStatus;
        this.userAccount = userAccount;
    }

    public boolean isLogingStatus() {
        return logingStatus;
    }

    public void setLogingStatus(boolean logingStatus) {
        this.logingStatus = logingStatus;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginState that = (LoginState) o;
        return logingStatus == that.logingStatus && Objects.equals(userAccount, that.userAccount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logingStatus, userAccount);
    }

    @Override
    public String toString() {
        return "LoginState{" +
                "logingStatus=" + logingStatus +
                ", userAccount=" + userAccount +
                '}';
    }
}
