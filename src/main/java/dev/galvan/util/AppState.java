package dev.galvan.util;
// carlos.galvanjr@revature.net

import java.util.*;

import dev.galvan.models.LocalStorage;

import static dev.galvan.util.AppStateUtil.*;
import static dev.galvan.util.BankAccountUtil.accountMenu;
import static dev.galvan.util.LogginUtil.logInLoop;

import static dev.galvan.util.RegisterUtil.registerLoop;

public class AppState {
    private static Scanner in = new Scanner(System.in);

    //    public Scanner in = new Scanner(System.in);
    public static void startApp() {

        LocalStorage localStorage = new LocalStorage(true);
        localStorage.setWelcome(true);
        boolean appState = true;
        boolean welcome = true;


        while (localStorage.isRunning()) {
            if (localStorage.isWelcome()) {
                welcomeMessage();
                localStorage.setWelcome(false);
            }
            try {
                int input = in.nextInt();

                switch (input) {
                    case 1:
                        dashedLine();
                        if (!localStorage.isLoggedIn()) {
                            localStorage = logInLoop(localStorage);
                        }
                        localStorage.setExitClause(true);
                        break;
                    case 2:
                        dashedLine();
                        localStorage = registerLoop(localStorage);
                        localStorage.setExitClause(true);
                        dashedLine();
                        break;
                    case 3:
                        dashedLine();
                        learnMore();
                        dashedLine();
                        localStorage.setWelcome(true);
//                        options();
                        break;
                    case 0:
                        System.out.println("BANK APPLICATION WILL NOW CLOSE | HAVE A GREAT DAY");
                        localStorage.setRun(false);
                        break;
                    default:
                        break;
                }
                /*if (input == 1) {
                    dashedLine();
                    if (!localStorage.isLoggedIn()) {
                        localStorage = logInLoop(localStorage);
                    }
//                    LoginState loginState = logInLoop(appState);
                    localStorage = accountMenu(localStorage);
                } else if (input == 2) {
                    dashedLine();
                    registerLoop(appState);
//                    System.out.println("You choose 2");
                } else if (input == 3) {
                    learnMore();
                    options();
                } else if (input == 0) {
                    System.out.println("BANK APPLICATION WILL NOW CLOSE | HAVE A GREAT DAY");
                    appState = false;
                } else if (input > 0 || input < 3) {

                    System.out.printf("Sorry %d is not a valid option. Try again%n", input);
                    options();
                }*/
            } catch (Exception e) {
                String input = in.next();
                System.out.printf("Sorry \'%s\' is not a valid option. Try again\n", input);
//                e.printStackTrace();
            }
        }
    }


}

