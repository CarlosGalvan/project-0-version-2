package dev.galvan.util;

import dev.galvan.daos.AccountAccess;
import dev.galvan.daos.AccountAccessImpl;
import dev.galvan.daos.BankTransactions;
import dev.galvan.daos.BankTransactionsImpl;
import dev.galvan.models.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static dev.galvan.util.AppStateUtil.*;

public class BankAccountUtil {
    private static Scanner in = new Scanner(System.in);

    //This method is the main menu for users who are logged in
    static LocalStorage accountMenu(LocalStorage localStorage) {

        BankTransactions bankTransactions = new BankTransactionsImpl();
        while (localStorage.isExitClause() || localStorage.isLoggedIn()) {
            if (localStorage.isWelcome()) {
                accountMenuOptions();
                localStorage.setWelcome(false);
            }
            try {
                int input = in.nextInt();

                switch (input) {
                    case 1:
                        //deposit
                         depositToBankAccount(chooseBankAccount(localStorage));
                         localStorage.setWelcome(true);
                         dashedLine();
                         break;
//                         localStorage.setExitClause(false);
                        //return localStorage.getUserAccount().setOpenAccounts(chooseBankAccount(localStorage));
                    case 2:
                        withdrawFromBankAccount(chooseBankAccount(localStorage), localStorage);
                        localStorage.setWelcome(true);
                        dashedLine();
                        break;
                    case 3:
                        double balance = bankTransactions.checkBalance(chooseBankAccount(localStorage));
                        System.out.printf("Your current balance is: $%.2f.%n", balance);
                        localStorage.setWelcome(true);
                        dashedLine();
                        break;
                    case 4:
                        BankAccount bankAccount = createBankAccount(localStorage.getUserAccount());
                        if (localStorage.getUserAccount().getOpenAccounts() == null) {
                            List<BankAccount> newAccounts = new ArrayList<>();
                            newAccounts.add(bankAccount);
                        } else if (localStorage.getUserAccount().getOpenAccounts() != null) {
                            List<BankAccount> storedAccounts = localStorage.getUserAccount().getOpenAccounts();
                            storedAccounts.add(bankAccount);
                        }
                        System.out.println();
                        accountMenuOptions();
                        System.out.println();
                        break;
                    case 5:
                        printTransactions(chooseBankAccount(localStorage), localStorage);
                        localStorage.setWelcome(true);
                        dashedLine();
                        break;
                    case 0:
                        localStorage.setLoggedIn(false);
                        localStorage.setExitClause(false);
                        localStorage.setWelcome(true);
                    System.out.println("Have a good day");
                    dashedLine();
                        return localStorage;
                    default:
                        break;

                }
            } catch (Exception e) {
                String input = in.next();
                System.out.printf("Sorry \'%s\' is not a valid option. Try again\n", input);
            }
        }

        return localStorage;
    }

    private static void printTransactions(BankAccount bankAccount, LocalStorage localStorage) {
        if (bankAccount != null) {
            BankTransactions bankTransactions = new BankTransactionsImpl();
            List<BankStatements> statements = bankTransactions.bankStatement(bankAccount);
            System.out.println("Bank Statements for Account named " + bankAccount.getAlias());
            statements.forEach( e -> System.out.println(e.toString()));
        }
    }

    private static void withdrawFromBankAccount(BankAccount bankAccount , LocalStorage localStorage) {
        if (bankAccount != null) {
            BankStatements bankStatements = new BankStatements();
            BankTransactions bankTransactions = new BankTransactionsImpl();
            double currentBalance = bankTransactions.checkBalance(bankAccount);

            try {
                System.out.println("How much do you want to withdraw from " + bankAccount.getAlias() + "?");
                double withdraw = in.nextDouble();
                if (withdraw <= currentBalance) {
                    bankStatements.setAccountNumber(bankAccount.getAccountNumber());
                    bankStatements.setUserAccountId(bankAccount.getUserAccountId());
                    bankStatements.setTransactionAmount(withdraw);
                    bankStatements.setTransactionDate(LocalDateTime.now());
                    bankStatements.setTransactionType("withdraw");
                    double correctBalance = bankTransactions.withdrawFunds(bankStatements, currentBalance);
                    double trueBalance = bankTransactions.checkBalance(bankAccount);
                    if (correctBalance == trueBalance) {
                        System.out.println("Withdraw Successful");
                        System.out.printf("Your new balance is: $%.2f.%n", trueBalance);

                    }
                } else {
                    System.out.println("Not enough money!");
                }

//                double newBalance = bankTransactions.depositFunds(bankStatements, bankAccount.getBalance());
            } catch (Exception e) {
                System.out.println("Not a a valid input");
            }

        }
    }

    private static BankAccount chooseBankAccount(LocalStorage localStorage) {
        BankAccount bankAccount = new BankAccount();
        AccountAccess accountAccess = new AccountAccessImpl();
        BankTransactions bankTransactions = new BankTransactionsImpl();
        List<BankAccount> bankAccountList = accountAccess.getAllAssociatedBankAccounts(localStorage.getUserAccount());
        int count = 0;
        if (bankAccountList.size() == 0) {
            System.out.println("No bank account found; Create a new account in the menu area");
            return null;
        }
        for (BankAccount ba :
                bankAccountList) {
            System.out.printf("[%d] %s %n", count, ba.getAlias());
            count++;

        }
        while (localStorage.isExitClause()) {
            try {
                int response = in.nextInt();
                if (bankAccountList.get(response) != null) {

                    BankAccount current = bankAccountList.get(response);

                    System.out.println(current.getAlias() + " Has been chosen");
                    return current;

                }
            } catch (Exception e) {
//                e.printStackTrace();
                System.out.println("Not a valid response; Try again");
            }

        }
        return bankAccount;
    }


    private static void depositToBankAccount(BankAccount bankAccount){
        if (bankAccount != null) {


            BankStatements bankStatements = new BankStatements();
            BankTransactions bankTransactions = new BankTransactionsImpl();

            try {
                System.out.println("How much do you want to deposit to " + bankAccount.getAlias());
                double deposit = in.nextDouble();
                bankStatements.setAccountNumber(bankAccount.getAccountNumber());
                bankStatements.setUserAccountId(bankAccount.getUserAccountId());
                bankStatements.setTransactionAmount(deposit);
                bankStatements.setTransactionDate(LocalDateTime.now());
                bankStatements.setTransactionType("Deposit");

                //this deposits amount
                //save recipeit to transactions
                double newBalance = bankTransactions.depositFunds(bankStatements, bankAccount.getBalance());
            } catch (Exception e) {
                System.out.println("Not a a valid input");
            }


        }
//        bankAccountList.get(response).setBalance(newBalance);

    }

    private static BankAccount createBankAccount(UserAccount userAccount) {
        boolean appState = false;
        BankAccount bankAccount = new BankAccount(userAccount.getAccountId());
        System.out.println("To create a new bank account we need some information:");
        System.out.println("What type of bank account? [0 = Savings | 1 = Checking]");
        while (!appState) {

            if (bankAccount.getAccountType() == null) {
//                String responce = in.nextLine();
//                if (!responce.isEmpty()) {
                try {
                    int input = Integer.parseInt(in.next());
                    if (input == 1) {
                        bankAccount.setAccountType("Checking");
                        System.out.println("You chose Checking, Now give this account a nickname: ");
                    } else if (input == 0) {
                        bankAccount.setAccountType("Savings");
                        System.out.println("You chose Savings, Now give this account a nickname: ");
                    } else if (input != 1 || input != 0) {
                        System.out.println("Not a valid input; Try again");
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    System.out.println("Not a valid input! try again");
                }
//                }
            } else if (bankAccount.getAlias() == null) {
                String input = in.nextLine();
                if (!input.isEmpty()) {
                    System.out.printf("Do you want to name this %s account %s? [ 1 = YES or 0 = NO]%n", bankAccount.getAccountType(), input);
                    try {
                        int verify = in.nextInt();
                        if (verify == 1) {
                            bankAccount.setAlias(input.trim());
                        } else if (verify == 0) {
                            System.out.println("Lets try this again");
                        }

                    } catch (Exception e) {
                        System.out.println("[ 1 = YES or 0 = NO]");
                    }

                }

            } else if (bankAccount.getAccountType() != null && bankAccount.getAlias() != null) {
                AccountAccess accountAccess = new AccountAccessImpl();
                bankAccount = accountAccess.createNewBankAccount(bankAccount);
                if (bankAccount.getAccountNumber() > 0) {
                    System.out.println("Account creation successful!");
                    return bankAccount;
//                    appState = !appState;
                } else {
                    System.out.println("Lets try again");
                    bankAccount = new BankAccount(userAccount.getAccountId());
                }
            }
        }
//        return false;
        return bankAccount;
    }

}
