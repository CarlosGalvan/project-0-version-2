package dev.galvan.util;

import dev.galvan.daos.AccountAccess;
import dev.galvan.daos.AccountAccessImpl;
import dev.galvan.models.LocalStorage;
import dev.galvan.models.LoginPackage;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static dev.galvan.util.AppStateUtil.options;
import static dev.galvan.util.BankAccountUtil.accountMenu;

/*

This class contains the logInLoop which is used to handle log in
 */
public class LogginUtil {
    private static Scanner in = new Scanner(System.in);

    public static LocalStorage logInLoop(LocalStorage localStorage) throws InterruptedException {
        System.out.println("ENTER CREDENTIALS TO LOG IN OR TYPE EXIT TO RETURN TO MAIN MENU");
        AccountAccess accountAccess = new AccountAccessImpl();
        boolean emailEntered = false;
        LoginPackage givenCredentials = new LoginPackage();
        System.out.println("ENTER EMAIL: ");

        localStorage.setExitClause(true);

        while (localStorage.isExitClause()) {

            String input = in.nextLine().trim();

            if (givenCredentials.getEmail() == null) {
                if (verifyEmailSyntax(input)) {
                    givenCredentials.setEmail(input);
//                    emailEntered = true;
//                    TimeUnit.SECONDS.sleep(2);
                    System.out.println("ENTER PASSWORD: ");
                } else if (!verifyEmailSyntax(input) & !input.isEmpty()) {
                    System.out.println("Invalid Email Syntax");
                }
            } else if (
                    givenCredentials.getEmail() != null && givenCredentials.getPassword() == null
//                    emailEntered && !passwordEntered
            ) {

                if (verifyPasswordSyntax(input)) {
                    givenCredentials.setPassword(input);
                } else if (givenCredentials.getPassword() == null) {
                    if (!verifyPasswordSyntax(input)) {
                        System.out.println("Invalid password syntax");
                    }
                    System.out.println("ENTER PASSWORD: ");
                }

            } else if (givenCredentials.getEmail() != null && givenCredentials.getPassword() != null) {
                localStorage = accountAccess.loginAccount(givenCredentials, localStorage);

                if (localStorage.isLoggedIn() && localStorage.getUserAccount().getAccountHolder() != null) {
                    //Logged in
                    System.out.printf("Welcome %s, what business do you have today?%n", localStorage.getUserAccount().getAccountHolder());
                    localStorage.setWelcome(true);
                    return accountMenu(localStorage);
                } else {
                    System.out.println("Wrong password or Email");
                    options();
                    localStorage.setExitClause(false);
                }
            }


            if (input.toUpperCase().matches("EXIT") || input.trim().equals("0")) {
                System.out.println("leaving log in page");
                localStorage.setWelcome(true);
                localStorage.setExitClause(false);
                return localStorage;
            }
        }
        return localStorage;
    }


    //helper method that verifies email syntax
//    https://mailtrap.io/blog/java-email-validation/
    static boolean verifyEmailSyntax(String input) {
        EmailValidator validator = EmailValidator.getInstance();
        // check for valid email addresses using isValid method
        return validator.isValid(input);
    }

    //Helper method to verify Password syntax
    public static boolean verifyPasswordSyntax(String password) {
        // Regex to check valid password.
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=!])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);
        if (password == null) {
            return false;
        }
        Matcher m = p.matcher(password);
        return m.matches();
    }
}
